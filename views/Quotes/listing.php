<h2>Liste des quotes du site</h2>
<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec consectetur odio malesuada augue posuere, vitae
    gravida nisl pellentesque. Mauris auctor dictum. Si vous souhaitez ajouter votre propre citation, cliquez sur ce lien :
    <a href="/quotes/listing#addQuote">Ajouter ma citation</a></p>

<?php foreach($quotes as $q): ?>
    <h3>
        Posté par <a href="/users/show/<?= $q->Users->id; ?>"><?= $q->Users->username; ?></a> le <?= $q->created; ?>
        <?php if(isset($Session->User) && $Session->User->type == 2): ?>
            | <a href="/quotes/delete/<?= $q->id; ?>">Supprimer</a>
        <?php endif; ?>
    </h3>

    <p>
        <?= nl2br($q->content); ?>
    </p>
<?php endforeach; ?>

<h2 id="addQuote">Ajouter votre quote</h2>

<?php if(Auth::isLoggued()): ?>
<form action="/quotes/add" method="post">
    <textarea name="content" id="quote" cols="30" rows="10" required="required" placeholder="Entrer votre citation ici, merci de ne poster que des choses drôles sous peine de devoir discuter 5 minutes avec Sarah"></textarea>

    <input type="submit" name="submit" />
</form>
<?php else: ?>
<p>Désolé, vous devez être connecté pour pouvoir ajouter une quote</p>
<?php endif; ?>