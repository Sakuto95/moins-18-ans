<h2>
    <?= $news->title; ?>
    <?php if(isset($Session->User) && $Session->User->type == 2): ?>
        <span class="date">
            <a href="/news/edit/<?= $news->id; ?>">Modifier</a> |
            <a href="/news/delete/<?= $news->id; ?>">Supprimer</a>
        </span>
    <?php endif; ?>
</h2>
<p><?= nl2br($news->content); ?></p>
<p>Posté le <?= $news->created; ?></p>