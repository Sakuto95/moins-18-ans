<h2>Ajouter / modifier une news</h2>
<p>Merci de remplir correctement tous les champs avant de poster sur envoyer</p>

<form action="/news/edit" method="post">
    <input type="text" name="title" placeholder="Titre de la news" value="<?= isset($news->title) ? $news->title : ''; ?>" />
    <textarea name="content" id="content" cols="30" rows="10"><?= isset($news->content) ? $news->content : ''; ?></textarea>
    <?php if(isset($news->id)): ?>
        <input type="hidden" name="id" value="<?= $news->id; ?>"/>
    <?php endif; ?>

    Afficher en tête de liste : <input type="checkbox" name="isImportant" <?= isset($news->isImportant) ? $news->isImportant : ''; ?>/> <br/><br/>

    <input type="submit" name="submit" />
</form>