<h2>Profil du membre <?= $user->username; ?></h2>
<table id="userDetail">
    <tr>
        <th>Nom d'utilisateur</th>
        <td><?= $user->username; ?></td>
    </tr>

    <tr>
        <th>Rang du membre</th>
        <td><?= $user->rank; ?></td>
    </tr>

    <tr>
        <th>Date d'inscription</th>
        <td><?= $user->created; ?></td>
    </tr>

    <tr>
        <th>&nbsp;</th>
    </tr>

    <tr>
        <th>Date d'anniversaire</th>
        <td><?= $user->birthdate; ?> (<?= date_diff(date_create($user->birthdate), date_create('today'))->y; ?> ans)</td>
    </tr>

    <tr>
        <th>Ville</th>
        <td><?= $user->city; ?></td>
    </tr>

    <tr>
        <th>Sexe</th>
        <td><?= $user->gender; ?></td>
    </tr>
</table>

<h2>Dernières photos de <?= $user->username; ?></h2>
<ul class="gallery">
    <?php foreach($galleries as $g): ?>
        <li>
            <a href="/galleries/show/<?= $g->id; ?>">
                <img src="assets/images/gallery/mini/<?= $g->id; ?>.png" alt="Photo de <?= $g->Users->username; ?>"/>
            </a>
            <em>Ajouté par <a href="/users/show/<?= $g->Users->id; ?>"><?= $g->Users->username; ?></a></em>
        </li>
    <?php endforeach; ?>
</ul>