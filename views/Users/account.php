<h2>Modifier votre profil</h2>
<p>C'est sur cette page que vous pourrez modifier toutes les informations concernant votre profil. Merci de rester correct dans ce que vous rentrez. Si vous ne souhaitez pas changer l'information actuelle, n'y touchez pas :)</p>

<form action="/users/account" method="post">
    <input type="email" name="mail" placeholder="<?= isset($user->mail) ? $user->mail : 'Votre nouvelle adresse mail'; ?>" />
    <input type="password" name="password" placeholder="Votre nouveau mot de passe" />
    <input type="date" name="birthdate" value="<?= isset($user->birthdate) ? $user->birthdate : ''; ?>"/>
    <input type="text" name="city" placeholder="<?= isset($user->city) ? $user->city : 'Ville de résidence'; ?>" />
    <input type="text" name="gender" placeholder='<?= isset($user->gender) ? $user->gender : 'Votre sexe (soyez inventif, évitez les "énormes")'; ?>' />

    <?php if($Session->User->type == 2): ?>
        <input type="text" name="rank" placeholder="<?= isset($user->rank) ? $user->rank : 'Rang de l\'utilisateur (purement visuel)'; ?>" />
        <input type="text" name="type" placeholder="1 = Membre, 2 = Administrateur" />
    <?php endif; ?>

    <input type="hidden" name="id" value="<?= $user->id; ?>"/>

    <input type="submit" name="submit"/>
</form>