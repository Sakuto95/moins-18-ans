<h2>Photo du membre <a href="/users/show/<?= $picture->Users->id; ?>"><?= $picture->Users->username; ?></a></h2>
<img src="assets/images/gallery/<?= $picture->id; ?>.png" alt="" class="gallery-image" />

<h2>Commentaires</h2>
<?php foreach($comments as $c): ?>
    <p>
        <?= $c->content; ?>
        <em class="align-right">
            Posté par <a href="/users/show/<?= $c->Users->id; ?>"><?= $c->Users->username; ?></a> le <?= $c->created; ?>
            <?php if(isset($Session->User) && $this->Session->User->type == 2): ?>
                | <a href="/commentaries/delete/<?= $c->id; ?>">Supprimer</a>
            <?php endif; ?>
        </em>
    </p>

    <?php endforeach; ?>

<h2>Ajouter un commentaire</h2>
<?php if(Auth::isLoggued()): ?>
    <form action="/commentaries/add" method="post">
        <textarea name="content" id="comment" cols="30" rows="10" required="required" placeholder="Entrer votre commentaire ici, merci de rester correct dans celui-ci et de ne pas demander de nude pic"></textarea>
        <input type="hidden" name="galleries_id" value="<?= $picture->id; ?>"/>
        <input type="submit" name="submit"/>
    </form>
<?php else: ?>
    <p>Désolé, vous devez être inscrit et connecté pour ajouter un commentaire sur cette photo</p>
<?php endif; ?>