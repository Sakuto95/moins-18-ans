<?php $news = Dispatcher::requestAction('News', 'getLastNews', [2]); ?>
<?php foreach($news as $n): ?>
    <h2><?= $n->title; ?> <span class="date">Posté le <?= $n->created; ?></span> </h2>
    <p><?= nl2br($n->content); ?></p>
<?php endforeach; ?>