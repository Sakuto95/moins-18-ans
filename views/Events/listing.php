<h2>Liste des évenements à venir</h2>
<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec consectetur odio malesuada augue posuere, vitae
    gravida nisl pellentesque. Mauris auctor dictum. Si vous souhaitez ajouter votre propre citation, cliquez sur ce lien :
</p>

<?php foreach($events as $e): ?>
    <h3>
        <?= $e->title; ?> <span class="date">qui aura lieu le <?= $e->start; ?></span>
    </h3>

    <p class="news-truncate">
        <?= nl2br($e->content); ?>
    </p>
    <span class="align-right">Posté le <?= $e->created; ?> | <a href="/events/show/<?= $e->id; ?>">Lire la suite</a></span>
<?php endforeach; ?>