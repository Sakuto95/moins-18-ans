<h2>Ajouter / modifier un évènement</h2>

<form action="/events/edit" method="post">
    <input type="text" name="title" placeholder="Titre de l'évenement" value="<?= isset($event->title) ? $event->title : ''; ?>"/>
    <textarea name="content" id="content" cols="30" rows="10" placeholder="Description de l'évenement"><?= isset($event->content) ? $event->content : ''; ?></textarea>
    <input type="date" name="start" value="<?= isset($event->start) ? $event->start : ''; ?>" />

    <?php if(isset($event)): ?>
        <input type="hidden" name="id" value="<?= $event->id; ?>"/>
    <?php endif; ?>

    <input type="submit" name="submit"/>
</form>