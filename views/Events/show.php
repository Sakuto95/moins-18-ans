<h2>
    <?= $event->title; ?>
    <span class="date">qui aura lieu le <?= $event->start; ?></span>
    <?php if(isset($Session->User) && $Session->User->type == 2): ?>
        <span class="date">
            <a href="/events/edit/<?= $event->id; ?>">Modifier</a> |
            <a href="/events/delete/<?= $event->id; ?>">Supprimer</a>
        </span>
    <?php endif; ?>
</h2>
<p><?= nl2br($event->content); ?></p>