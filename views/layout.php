<?php
$importantNews = Dispatcher::requestAction('News', 'getImportantNews');
$anniversary = Dispatcher::requestAction('Users', 'getAnniversary');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <link rel="stylesheet" type="text/css" media="screen" href="/assets/css/style.css"/>
    <title><?= isset($title_for_layout) ? $title_for_layout : 'Salon #-18ans Epiknet.org'; ?></title>
    <base href="<?= WEBSITE; ?>/">
</head>
<body>
<div id="indexhtml">
    <div id="content">
        <ul id="menu">
            <?php if(Auth::isLoggued()): ?>
                <li>Bienvenue <a href="/users/show/<?= $Session->User->id; ?>"><?= $Session->User->username; ?></a></li>
                <li><a href="/users/account">Mon compte</a></li>
                <li><a href="/users/logout">Déconnexion</a></li>
            <?php else: ?>
                <li><a href="/users/register">Inscription</a></li>
                <li><a href="/users/login">Connexion</a></li>
            <?php endif; ?>
        </ul>

        <div class="separateur"></div>

        <a href="/" id="logo">-18 ans Epiknet</a>

        <ul id="speedbarre">
            <a href="/" class="home">Accueil</a>

            <li><a href="/news/listing">News</a></li>
            <li><a href="/galleries/listing">Photomaton</a></li>
            <li><a href="/quotes/listing">Quotes</a></li>
            <li><a href="/events/listing">Evenements</a></li>
            <li><a href="/faq/listing">FAQ</a></li>
            <li><a href="/pages/contact">Contact</a></li>
        </ul>

        <div class="separateur"></div>

        <div class="breaknews"><span>ACTUALITE : </span> <?= $importantNews->title; ?> <a href="/news/show/<?= $importantNews->id; ?>">Lire la suite</a>
        </div>
        <div class="separateurdot"></div>

        <div id="body">
            <div id="left">
                <?php $this->Session->getFlash(); ?>
                <?= $content_for_layout; ?>
            </div>
            <div id="right">
                <?php if(isset($contextualMenu)): ?>
                    <h2>Menu contextuel</h2>
                    <?= $contextualMenu; ?>
                <?php endif; ?>

                <h2>Informations complémentaires</h2>
                <?php if(count($anniversary) == 0): ?>
                    Aucun anniversaire aujourd'hui <br/>
                <?php else: ?>
                    Anniversaires du jour :
                    <?php foreach ($anniversary as $a): ?>
                        <a href="/users/show/<?= $a->id; ?>"><?= $a->username; ?></a>
                    <?php endforeach; ?>
                    <br/>
                <?php endif; ?>
            </div>
            <div class="clear"></div>
        </div>
        <div id="footer">
            Développement et Design par <a href="http://www.ramelot-loic.be" title="Développeur web Belgique">RAMELOT Loïc</a>
        </div>
    </div>
    <?php debug($_SESSION); ?>
</div>
</body>
</html>
