<h2>Foire aux questions</h2>
<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Alias aliquid aspernatur cupiditate deserunt dolorem dolores dolorum earum enim fugit in maiores nemo porro, quod repellendus similique velit veniam vitae voluptatem!</p>

<?php foreach ($FAQ as $f): ?>
    <h2>
        <?= $f->question; ?>
        <?php if(isset($Session->User) && $Session->User->type == 2): ?>
            <span class="date">
                <a href="/faq/edit/<?= $f->id; ?>">Modifier</a> |
                <a href="/faq/delete/<?= $f->id; ?>">Supprimer</a>
            </span>
        <?php endif; ?>
    </h2>
    <p><?= $f->answer; ?></p>
<?php endforeach; ?>