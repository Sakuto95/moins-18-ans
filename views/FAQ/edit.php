<h2>Ajouter / modifier une question</h2>

<form action="/faq/edit" method="post">
    <input type="text" name="question" placeholder="Intitulé de la question" value="<?= isset($FAQ->question) ? $FAQ->question : ''; ?>"/>
    <textarea name="answer" id="answer" cols="30" rows="10"><?= isset($FAQ->answer) ? $FAQ->answer : ''; ?></textarea>

    <?php if(isset($FAQ)): ?>
        <input type="hidden" name="id" value="<?= $FAQ->id; ?>"/>
    <?php endif; ?>

    <input type="submit" name="submit"/>
</form>