<?php
define('WEBSITE', '');
define('ROOT', dirname(__FILE__));

require(ROOT.'/core/Session.php');
require(ROOT.'/core/functions.php');
require(ROOT.'/config/DatabaseConfiguration.php');
require(ROOT.'/core/Database.php');
require(ROOT.'/core/Auth.php');
require(ROOT.'/controllers/Controller.php');
require(ROOT.'/models/Model.php');
require(ROOT.'/core/Dispatcher.php');

$dispatcher = new Dispatcher();
$Auth = new Auth($dispatcher);
$dispatcher->dispatch();