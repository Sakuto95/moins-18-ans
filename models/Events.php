<?php 

class Events extends Model {
    public function findActive() {
        $req = $this->bdd->query("SELECT * FROM events WHERE start > CURDATE()");
        $req->setFetchMode(PDO::FETCH_OBJ);
        return $req->fetchAll();
    }
}