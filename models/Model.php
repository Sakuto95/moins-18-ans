<?php

/**
 * Class Model
 */
class Model
{
    protected $currentClass;
    protected $bdd;
    protected $primaryKey = 'id';

    /**
     * Basic constructor, initialize the database object
     */
    public function __construct()
    {
        $this->currentClass = strtolower(get_class($this));
        $this->bdd = Database::getInstance();
    }

    /**
     * Execute a hand made request
     * @param String $request The request that need to be executed
     * @param int $noFetch Return a fetched result or not
     * @return array|PDOStatement Result of the request
     */
    public function preparedRequest($request, $params) {
        $req = $this->bdd->prepare($request);
        $req->execute($params);

        $req->setFetchMode(PDO::FETCH_OBJ);

        return $req->fetchAll();
    }

    public function request($request, $noFetch = 0) {
        $req = $this->bdd->query($request);
        $req->setFetchMode(PDO::FETCH_OBJ);
        if(!$noFetch) {
            return $req->fetchAll();
        }
        return $req;
    }

    /**
     * Find every data related to the args passed
     * @param array $args Array of the params to pass to the where request
     * @param int $limit The max limit for the request
     * @param string $orderBy Clause used to group
     * @return array Result of the request
     */
    public function findAll($args = null, $limit = null, $orderBy = null)
    {
        $cond = '';
        $fields = [];

        if ($args) {
            $args = $this->parseArguments($args);
            $cond = $args[0];
            $fields = $args[1];
        }

        $limit = ($limit) ? " LIMIT $limit" : "";
        $orderBy = ($orderBy) ? " ORDER BY $orderBy" : "ORDER BY {$this->primaryKey} DESC";

        $req = $this->bdd->prepare("SELECT * FROM {$this->currentClass} $cond $orderBy $limit");
        $req->execute($fields);

        $req->setFetchMode(PDO::FETCH_OBJ);

        $data = $req->fetchAll();

        if(!empty($this->hasOne)) {
            foreach ($data as $d) {
                foreach ($this->hasOne as $j) {
                    $this->loadModel($j);
                    $foreignKey = strtolower($j . '_id');

                    $d->$j = $this->$j->find($d->$foreignKey);
                    unset($d->$foreignKey);
                }
            }
        }

        return $data;
    }

    /**
     * Return a record per id
     * @param $id Id of the record
     * @return array Array with the data of the selected record
     */
    public function find($id)
    {
        return current($this->findAll([$this->primaryKey => $id]));
    }

    /**
     * Remove a record or every records (depending on the id)
     * @param int $id Remove a record per id, if null, remove everything
     */
    public function delete($id = null)
    {
        if($id) {
            $id = implode(',', $id);
            $cond = "WHERE {$this->primaryKey} IN($id)";
        }

        $this->bdd->exec("DELETE FROM {$this->currentClass} $cond");
    }

    /**
     * Parse arguments to create a correct where clause
     * @param $args Array with [arguments => value]
     * @return array The condition and the list of fields
     */
    private function parseArguments($args)
    {
        $cond = [];
        $fields = [];

        if ($args) {

            foreach ($args as $r => $k) {
                $cond[] = "$r = ?";
                $fields[] = $k;
            }

            $cond = " WHERE " . implode(' AND ', $cond);
        } else {
            $cond = '';
        }

        return [$cond, $fields];
    }

    /**
     * Use the update or create function depending of the presence of
     * the id index in the array
     * @param $args Fields to save
     */
    public function save($args)
    {
        unset($args['submit']);

        if(isset($args['password']))
            $args['password'] = sha1($args['password']);

        if(array_key_exists($this->primaryKey, $args)) {
            $this->update($args);
            return $args['id'];
        } else {
            $this->create($args);
            return $this->bdd->lastInsertId();
        }
    }

    /**
     * Update an element with an id defined
     * @param $args The fields to update
     */
    public function update($args) {
        $id = $args[$this->primaryKey];
        unset($args[$this->primaryKey]);

        foreach($args as $r => $k) {
            $fieldsList[] = "$r = ?";
            $valuesList[] = $k;
        }

        $valuesList[] = $id;

        $updateList = implode(',', $fieldsList);

        $this->bdd->prepare("UPDATE {$this->currentClass} SET $updateList WHERE {$this->primaryKey}=?")
                  ->execute($valuesList);
    }

    /**
     * Create an element
     * @param $args The fields to insert
     */
    public function create($args) {
        $req = $this->bdd->query("SHOW COLUMNS FROM {$this->currentClass} LIKE '%created%'");
        if($req->rowCount() == 1) {
            $args['created'] = date("Y-m-d H:i:s");            
        }

        $fieldsList = implode(',', array_keys($args));
        $valuesListWildcard = $this->createStatementWithJoker(sizeof($args), '?');
        $valuesList = array_values($args);
        

        $this->bdd->prepare("INSERT INTO {$this->currentClass} ($fieldsList) VALUES ($valuesListWildcard)")
                  ->execute($valuesList);

        return $this->bdd->lastInsertId();
    }

    /**
     * Return a string formed of wildChat separated by ,
     * @param $number Number of wildchar
     * @param $wildchar Character to use
     * @return string wildchar,wlidchar,...
     */
    protected function createStatementWithJoker($number, $wildchar)
    {
        $string = [];
        for($i = 0; $i < $number; $i++) {
            $string[] = $wildchar;
        }
        return implode(',', $string);
    }

    /**
     * Perform a research on every fields on a database
     * @param $search Text to search
     * @return array Result
     */
    public function findOnAllTable($search) {
        $query = "SELECT * FROM {$this->currentClass} WHERE ";
        $fields = [];

        $fieldsRequest = $this->bdd->query("SHOW COLUMNS FROM {$this->currentClass}");

        if($fieldsRequest->rowCount() > 0){
            foreach($fieldsRequest->fetchAll() AS $field) {
                $fields[] = $field[0]. " LIKE(:value)";
            }
        }

        $query .= implode(" OR ", $fields);

        if(!empty($limit)) $query .= " LIMIT $limit";

        $req = $this->bdd->prepare($query);
        $req->execute(['value' => "%$search%"]);

        $req->setFetchMode(PDO::FETCH_OBJ);
        return $req->fetchAll();
    }

    public function loadModel($model)
    {
        $model = ucfirst($model);

        require_once(ROOT . '/models/' . $model . '.php');
        $this->$model = new $model();
    }
} 