<?php
exec("chcp 65001");

if($argc != 3) {
    echo "Bienvenue sur l'outil de création du Framework de Loïc RAMELOT \r\n";
    echo "Afin de continuer à vous servir de cet outil, merci d'utiliser correctement la ligne de commande, voici les fonctions disponibles : \r\n\r\n ";
    echo "\t Créer un controller : php console.php controller nomDuControlleur \r\n";
    echo "\t Créer un modèle : php console.php model nomDuModele \r\n";
    echo "\t Créer une nouvelle action : php console.php action nomAction \r\n";

    die();
}

$fileName = ucfirst($argv[2]);

function createController($fileName, $isAction = 0) {
    $actionsFunction = "\t";
    if($isAction) {
        echo "Voulez vous créer des actions par défaut pour ce controlleur ? Si oui entrez les, sinon appuyer sur Entrer : ";

        $actions = trim(fgets(STDIN));

        if(!empty($actions)) {
            $actions = explode(',', $actions);
            $actionsFunction = '';

            foreach($actions as $a) {
                $actionsFunction .= "\tpublic function $a() {\r\n\t\t\r\n\t}\r\n\r\n";
                createView($fileName, $a);
            }
        }
    }

    $file = fopen("controllers/{$fileName}Controller.php", 'w');
    fwrite($file, "<?php \r\n\r\nclass {$fileName}Controller extends Controller {\r\n$actionsFunction}");
    fclose($file);
}

function createModel($fileName) {
    echo "Le modèle $fileName a t-il des jointures avec d'autres modèles ? Si oui, entrez les séparer par une virgule, sinon, appuyez sur Enter : ";
    $jointures = trim(fgets(STDIN));

    if(!empty($jointures)) {
        $jointures = explode(',', $jointures);

        foreach($jointures as $id => $t) {
            $t = ucfirst($t);
            $jointures[$id] = "'$t'";
        }
    }

    $jointures = !empty($jointures) ? "\t public \$hasOne = [".implode(', ', $jointures)."];\r\n" : "";

    $file = fopen("models/{$fileName}.php", 'w');
    fwrite($file, "<?php \r\n\r\nclass {$fileName} extends Model {\r\n$jointures\r\n}");
    fclose($file);
}

function createView($controller, $action = null) {
    if(empty($action)) {
        mkdir("views/$controller");
    } else {
        touch("views/$controller/$action.php");
    }
}

switch($argv[1]) {
    case 'controller':
        createController($fileName);
        break;

    case 'model':
        createModel($fileName);
        break;

    case 'action':
        createView($fileName);
        createController($fileName, 1);
        createModel($fileName);
        break;
}