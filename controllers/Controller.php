<?php

class Controller
{
    private $args = [];
    private $isRendered = false;
    public $Session;

    /**
     * Load the right Model depending on the controller
     */
    public function __construct($sessionObject = null)
    {
        $this->Session = $sessionObject;

        $model = str_replace('Controller', '', get_class($this));

        if($model != 'Pages')
        $this->loadModel($model);

        // Load related Model
        if(!empty($this->$model->hasOne)) {
            foreach($this->$model->hasOne AS $d) {
                $this->loadModel($d);
            }
        }
    }

    /**
     * Include and return instance of the model passed in parameter
     * @param $model Model name to return
     */
    public function loadModel($model)
    {
        $model = ucfirst($model);

        require_once(ROOT . '/models/' . $model . '.php');
        $this->$model = new $model();
    }

    /**
     * Set a value for the view
     * @param $name Name of the variable
     * @param $value Value of the variable
     */
    public function set($name, $value)
    {
        $this->args[$name] = $value;
    }

    /**
     * Include and render a view
     * @param $action
     * @param null $controller
     */
    public function render($action, $controller = null)
    {
        if (!$this->isRendered) {
            if ($controller == null) {
                $controller = $this->toControllerViewName(get_class($this));
            }

            extract($this->args);
            $Session = $this->Session;
            ob_start();

            include('views/' . $controller . '/' . $action . '.php');
            $content_for_layout = ob_get_clean();

            if(preg_match("#admin_#i", $action))
                require('views/admin.php');
            else
                require('views/layout.php');

            $this->isRendered = true;
        }

    }

    /**
     * Redirect to another page
     * @param $url
     */
    public function redirect($url)
    {
        header("Location: " . WEBSITE . "$url");
    }

    /**
     *
     * @param $controller
     * @return string
     */
    private function toControllerViewName($controller)
    {
        $controller = str_replace('Controller', '', $controller);
        return strtolower($controller);
    }
} 