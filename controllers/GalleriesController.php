<?php 

class GalleriesController extends Controller {
	public function listing() {
		$data = $this->Galleries->findAll();
        $this->set('galleries', $data);
	}

	public function show($id = null) {
        $this->loadModel('Commentaries');

		$picture = $this->Galleries->find($id);
        $comments = $this->Commentaries->findAll(['galleries_id' => $id]);

        $this->set('picture', $picture);
        $this->set('comments', $comments);
	}

	public function add() {
		
	}

	public function admin_listing() {
		
	}

}