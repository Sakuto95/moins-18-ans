<?php
class CommentariesController extends Controller {
    public function add() {
        if(!empty($_POST['content'])) {
            $_POST['users_id'] = $this->Session->User->id;

            $this->Commentaries->save($_POST);
            $this->Session->setFlash("Votre commentaire a bien été ajouté, merci !");
            $this->redirect('/galleries/show/'.$_POST['galleries_id']);
        }
    }

    public function delete($id) {
        $this->Commentaries->delete([$id]);
        $this->Session->setFlash("Le commentaire a bien été supprimé");
        $this->redirect($_SERVER['HTTP_REFERER']);
    }
}