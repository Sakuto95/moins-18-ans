<?php 

class UsersController extends Controller {
    public function register() {
        $data = $_POST;

        // On a envoyé et donc on tente de s'inscrire
        if(!empty($data)) {
            if(count($this->Users->findAll(['mail' => $data['mail']])) != 0) $errors[] = 'Cette adresse email est déjà présente';

            if(empty($errors)) {
               unset($data['passConf']);

                $this->Users->save($data);

                $this->Session->setFlash("Vous êtes correctement inscrit sur le site, merci de votre inscription et à très vite");
                $this->redirect('/');
            } else {
                $this->Session->setFlash("Merci de régler les erreurs suivantes afin de pouvoir continuer votre inscription :  <br> " . implode($errors, '<br>'), 0);
            }
        }
    }

    public function login() {
        if(isset($_POST['submit'])) {
            if ($user = Auth::login($_POST['username'], $_POST['password'])) {
                $user = $this->Users->find($user);

                $_SESSION['User'] = $user;

                $this->Session->setFlash("Vous êtes correctement connecté, bienvenue !");
                $this->redirect("/");
            } else {
                $this->Session->setFlash("Une erreur est survenue, merci de vérifier vos informations de connexions", 0);
            }
        }
    }

    public function logout() {
        unset($this->Session->User);
        $this->Session->setFlash("Vous êtes correctement déconnecté");
        $this->redirect("/users/login");
    }

    public function show($id = null) {
        $this->loadModel('Galleries');

        $user = $this->Users->find($id);
        $galleries = $this->Galleries->findAll(['users_id' => $id]);

        $this->set('user', $user);
        $this->set('galleries', $galleries);
    }

    public function getAnniversary() {
        $data = $this->Users->request("SELECT username, id FROM users WHERE MONTH(birthdate) = MONTH(NOW()) AND DAY(birthdate) = DAY(NOW())");
        return $data;
    }

    public function account($id = null) {
        if(isset($_POST['submit'])) {
            if($_POST['id'] != $this->Session->User->id && $this->Session->User->type != 2) {
                $this->Session->setFlash("Cette tentative de piratage a été enregistrée {$this->Session->User->username}, attention à toi :)", 0);
                $this->redirect('/');
                die();
            }

            $this->Session->setFlash("Votre profil a bien été modifié", 0);
            $this->Users->save(array_filter($_POST));
        }

        if($id && $this->Session->User->type != 2) {
            $this->Session->setFlash("Cette tentative de piratage a été enregistrée {$this->Session->User->username}, attention à toi :)", 0);
            $this->redirect('/');
            die();
        }

        if(!$id) $id = $this->Session->User->id;

        $data = $this->Users->find($id);
        $this->set('user', $data);
    }
}