<?php 

class NewsController extends Controller {
	public function listing() {
		$data = $this->News->findAll();
        $this->set('news', $data);

        if(isset($this->Session->User) && $this->Session->User->type == 2) {
            $contextualMenu = '<a href="/news/edit/">Ajouter une news</a>';
            $this->set('contextualMenu', $contextualMenu);
        }
	}

	public function show($id = null) {
		$data = $this->News->find($id);
        $this->set('news', $data);
	}

	public function edit($id = null) {
	    if($id) {
            $data = $this->News->find($id);
            $data->isImportant = $data->isImportant == 1 ? 'checked="checked"' : '';
            $this->set('news', $data);
        }

        if(isset($_POST['submit'])) {
            $_POST['isImportant'] = isset($_POST['isImportant']) ? 1 : 0;

            $newsId = $this->News->save($_POST);
            $this->Session->setFlash("La news a bien été ajoutée / modifiée");
            $this->redirect('/news/show/'.$newsId);
        }
	}

    public function getLastNews($number = 2) {
        return $this->News->findAll([], 2);
    }

    public function getImportantNews() {
        $data = $this->News->findAll(['isImportant' => 1], null, 'id DESC');
        return current($data);
    }
}