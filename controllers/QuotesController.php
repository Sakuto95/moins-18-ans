<?php 

class QuotesController extends Controller {
	public function add() {
		if(isset($_POST['content'])) {
            $_POST['users_id'] = $this->Session->User->id;
            $this->Quotes->save($_POST);

            $this->Session->setFlash("Votre quote a bien été ajoutée sur le site");
        }

        $this->redirect('/quotes/listing');
	}

    public function delete($id = null) {
        $this->Quotes->delete([$id]);
        $this->Session->setFlash("Cette quote a bien été supprimée");
        $this->redirect('/quotes/listing');
    }

	public function listing() {
		$data = $this->Quotes->findAll();
        $this->set('quotes', $data);
	}

}