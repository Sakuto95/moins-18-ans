<?php 

class EventsController extends Controller {
	public function listing() {
		$data = $this->Events->findActive();
        $this->set('events', $data);

        if(isset($this->Session->User) && $this->Session->User->type == 2) {
            $contextualMenu = '<a href="/events/edit/">Ajouter un evenement</a>';
            $this->set('contextualMenu', $contextualMenu);
        }
	}

	public function edit($id = null) {
		if($id) {
            $event = $this->Events->find($id);
            $this->set('event', $event);
        }

        if(isset($_POST['submit'])) {
            $idEvent = $this->Events->save($_POST);
            $this->Session->setFlash("L'évènement a bien été modifié / ajouté");
            $this->redirect('/events/show/'.$idEvent);
        }
	}

	public function show($id = null) {
		$data = $this->Events->find($id);
        $this->set('event', $data);
	}

    public function delete($id) {
        $this->Events->delete([$id]);
        $this->Session->setFlash("Cet évenement a été supprimé");
        $this->redirect('/events/listing');
    }
}