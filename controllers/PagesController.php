<?php
/**
 * Created by PhpStorm.
 * User: Sakuto
 * Date: 26-03-15
 * Time: 19:24
 */

class PagesController extends Controller {

	

	public function admin_home() {
		$this->loadModel('Users');
		$this->loadModel('Seekers');
		$this->loadModel('Company');
		$this->loadModel('Offers');

		$users = count($this->Users->findAll());
		$seekers = count($this->Seekers->findAll());
		$company = count($this->Company->findAll());
		$offers = count($this->Offers->findAll());
		echo $users;
		$this->set("users", $users);
		$this->set("seekers", $seekers);
		$this->set("company", $company);
		$this->set("annonces", $offers);
	}

	public function getStats() {
		$this->loadModel('Users');
		$this->loadModel('Seekers');
		$this->loadModel('Company');
		$this->loadModel('Offers');

		$users = count($this->Users->findAll());
		$seekers = count($this->Seekers->findAll());
		$company = count($this->Company->findAll());
		$offers = count($this->Offers->findAll(["status" => -1]));
		$offers1 = count($this->Offers->findAll(["status" => 0]));
		$offers2 = count($this->Offers->findAll(["status" => 1]));
		$result = [$offers,$company,$seekers,$users,$offers1,$offers2];
		return $result; 
	}


	public function getNumberOfNews() {
		$this->loadModel('Offers');
		$this->loadModel('Submissions');
		$requestNumberSubmissions = count($this->Submissions->findAll(["status" => 0]));
		$requestNumberOffers = count($this->Offers->findAll(["status" => 0]));
		return [$requestNumberSubmissions, $requestNumberOffers];
	}


	public function home() {
		$this->loadModel("Offers");
		$offers = $this->Offers->findAll();
		$this->set("offers", $offers);
	}

} 