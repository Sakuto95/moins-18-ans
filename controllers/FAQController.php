<?php 

class FAQController extends Controller {
	public function listing() {
		$data = $this->FAQ->findAll();
        $this->set('FAQ', $data);

        if(isset($this->Session->User) && $this->Session->User->type == 2) {
            $contextualMenu = '<a href="/faq/edit/">Ajouter une question</a>';
            $this->set('contextualMenu', $contextualMenu);
        }
	}

	public function edit($id = null) {
		if($id) {
            $faq = $this->FAQ->find($id);
            $this->set('FAQ', $faq);
        }

        if(isset($_POST['submit'])) {
            $this->FAQ->save($_POST);
            $this->Session->setFlash("Votre question a bien été ajoutée / modifiée");
            $this->redirect('/faq/listing');
        }
	}

	public function delete($id) {
		$this->FAQ->delete([$id]);
        $this->Session->setFlash("La question a bien été supprimée");
        $this->redirect('/faq/listing');
	}

}