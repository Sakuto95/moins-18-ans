<?php

class Dispatcher
{
    private $controller;
    public $Session;
    private $action;
    private $args = [];

    /**
     * Construct the element
     * Retrieve the controller and the action depending of the url
     */
    public function __construct()
    {
        $this->Session = Session::getInstance();

        $args = explode('/', str_replace(WEBSITE, '', $_SERVER['REQUEST_URI']));
        array_shift($args);

        $this->controller = array_shift($args);
        $this->action = array_shift($args);
        $this->args = $args;

        if (empty($this->action)) {
            if (empty($this->controller)) {
                $this->controller = 'pages';
            }
            $this->action = 'home';
        }
    }

    /**
     * Include the correct controller and action depending of the action / controller
     */
    public function dispatch()
    {
        $controller = $this->loadController($this->controller);
        if(Auth::isAuthorized()) {

            if($this->controller != 'pages') {
                call_user_func_array([$controller, $this->action], $this->args);
            }

            $controller->render($this->action, $this->controller);
        } else {
            $controller->Redirect('/pages/404');
        }
    }

    /**
     * Include the controller retrieved by the controller
     * @param $controllerToUse String Name of the controller
     * @return Instance of the controller
     */
    private function loadController($controllerToUse)
    {
        $name = ucfirst($controllerToUse) . 'Controller';
        require_once('controllers/' . $name . '.php');
        $controller = new $name($this->Session);
        return $controller;
    }

    /**
     * Perform a request from view / controller on a controller / action
     * @param $controller Controller to include
     * @param $action Action to perform
     * @param array $args Arguments for the action
     * @return mixed Result of the method
     */
    public static function requestAction($controller, $action, $args = [])
    {
        $controller = ucfirst($controller) . 'Controller';
        require_once('controllers/' . $controller. '.php');
        $actionController = new $controller();
        return call_user_func_array([$actionController, $action], $args);
    }

    /**
     * Return the current controller
     * @return string
     */
    public function getController()
    {
        return $this->controller;
    }

    /**
     * Return the current action
     * @return string
     */
    public function getAction()
    {
        return $this->action;
    }
} 