<?php

class Session
{
    private $message = "messageFlash";
    private $messageIncrement = "messageFlashIncrement";
    private static $_instance;

    public static function getInstance()
    {
        if (is_null(self::$_instance)) {
            self::$_instance = new Session();
        }

        return self::$_instance;
    }

    public
    function __construct()
    {
        session_start();
    }

    public
    function setFlash($message, $messageIncrement = 1)
    {
        $_SESSION[$this->message] = $message;
        $_SESSION[$this->messageIncrement] = $messageIncrement;
    }

    public
    function getFlash()
    {
        if (isset($_SESSION[$this->message]) && $_SESSION[$this->messageIncrement] == 0) {
            $message = $_SESSION[$this->message];

            include(ROOT . '/views/flashMessage.php');

            unset($_SESSION[$this->message]);
            unset($_SESSION[$this->messageIncrement]);
        } else if (isset($_SESSION[$this->message]) && $_SESSION[$this->messageIncrement] == 1) {
            $_SESSION[$this->messageIncrement] = 0;
        }
    }

    public
    function __get($property)
    {
        return isset($_SESSION[$property]) ? $_SESSION[$property] : null;
    }

    public
    function __set($property, $value)
    {
        if (!method_exists($this, $property)) {
            $_SESSION[$property] = $value;
        }
    }

    public
    function __unset($property)
    {
        unset($_SESSION[$property]);
    }

    public function __isset($property)
    {
        return isset($_SESSION[$property]);
    }
}