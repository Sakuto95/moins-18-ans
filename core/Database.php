<?php

class Database {
    private static $instance;
    private static $bdd;

    public static function getInstance()
    {
        if (is_null(self::$instance)) {
            self::$instance = new Database();
        }

        return self::$bdd;
    }

    public function __construct() {
        $configuration = DatabaseConfiguration::$databases;
        self::$bdd = new PDO("mysql:host={$configuration['host']};dbname={$configuration['database']};charset=utf8", $configuration['user'], $configuration['password'], array(PDO::ATTR_ERRMODE => PDO::ERRMODE_WARNING));
    }
} 